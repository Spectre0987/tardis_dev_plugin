
(function(){

    var exportButton;
    var makeIntAction;
    var genHex;
    var makeControl;
    var addSonicCPButton;
    var exportSonicCPButton;
    
    var editCallback;

    var tardisMenu;

    const controlNameCubeProperty = new Property(Cube, 'string', 'tardis_control', {});
    const sonicPartCubeProperty = new Property(Cube, 'string', 'tardis_sonic_cp', undefined);

    const ControlOptions = {
        "tardis:throttle" : "Throttle",
        "tardis:handbrake" : "Handbrake",
        "tardis:refueler" : "Refueler",
        "tardis:stabilizers" : "Stabilizers",
        "tardis:increment" : "Increment Modifier",
        "tardis:x" : "X Control",
        "tardis:y" : "Y Control",
        "tardis:z" : "Z Control",
        "tardis:land_type" : "Landing Type Selector",
        "tardis:facing" : "Facing Selector",
        "tardis:randomizer" : "Randomizer",
        "tardis:dimension" : "Dimensional Control",
        "tardis:telepathics" : "Telepathic Circuits",
        "tardis:fast_return" : "Fast Return Switch",
        "tardis:communicator" : "Communicator",
        "tardis:sonic_port" : "Sonic Port",
        "tardis:monitor": "Console Monitor"
    };

    const SonicOptions = {
        'emitter' : 'Emitter',
        'activator':'Activator',
        'handle':'Handle',
        'cap':'Cap'
    };

    Plugin.register('control_plugin', {
        title: 'TARDIS Mod Dev Tools',
        author: 'LilithMotherOfAll',
        description: 'General Dev tools for the TARDIS Mod. Lets you add control hitboxes to consoles, and other helpful utilites',
        version: '2.0',
        variant: 'both',
        onunload(){
            exportButton.delete();
            makeIntAction.delete();
            genHex.delete();
            makeControl.delete();
            addSonicCPButton.delete();

            tardisMenu.delete();
            Blockbench.removeListener("finished_edit", editCallback);
        },
        onload(){
            
            //Add event handler
            Blockbench.on("finished_edit", editCallback = function(event){
                const elements = event.aspects.elements;
                if(elements){
                    elements.forEach(ele => {
                        if(ele.tardis_control != ''){ //If this has a control element
                            fixControl(ele);
                        }
                    });
                }
                else console.log(event.aspects);
            });

            //Add Export button
            exportButton = new Action('tardis_control_export', {
                name: "Export TARDIS Control Json",
                click: function(){
                    new Dialog("control_console", {
                        title: "Enter Console Registry Name",
                        form: {
                            console:{
                                type: 'text',
                                Label: 'Console Registry Name'
                            }
                        },
                        onConfirm: function(data){
                            Blockbench.export({
                                type: 'json',
                                content: exportControls(data.console),
                                extensions: ['json']
                            }, function(){});
                        }
                    }).show();
                }
            });
            MenuBar.addAction(exportButton, 'file.export');

            exportSonicCPButton = new Action('tardis_sonic_cp_export', {
                name: 'Sonic Connection',
                click: function(){
                    
                    new Dialog('tardis_sonic_cp_export_dia', {
                        title: "Export Sonic Connection File",
                        form: {
                            'sonic_slot':{
                                label: "Sonic Part Slot",
                                type: 'select',
                                options: SonicOptions
                            }

                        },
                        onConfirm: function(data){
                            Blockbench.export({
                                type: 'json',
                                content: exportSonicCP(Project.export_path, data.sonic_slot),
                                extensions: ['json']
                            });
                        }
                    }).show();
                }
            });
            MenuBar.addAction(exportSonicCPButton, 'file.export');
            
            
            addSonicCPButton = new Action('tardis_sonic_cp', {
                name: 'Add Sonic connection Point',
                icon: 'fa-cubes',
                click: function(){
                    new Dialog('tardis_sonic_cp', {
                        title: 'Add New Sonic Connection point',
                        onConfirm : function(data){
                            const cube = new Cube({
                                name: SonicOptions[data.connection],
                                from: [0, 0, 0],
                                to: [0, 0, 0]
                            })
                            cube.sonicPartCubeProperty = data.connection
                            cube.init();
                        },
                        form: {
                            'connection':{
                                type: 'select',
                                options: SonicOptions
                            }
                        }
                    }).show();
                }
            });

            makeIntAction = new Action('control_make_int', {
                name: 'Make Decimals Whole',
                icon: 'fa-cubes',
                click: function(){
                   Undo.initEdit({elements: Cube.all});
                   Cube.all.forEach(cube => {
                    var sizeX = cube.from[0] - cube.to[0];
                    var sizeY = cube.from[1] - cube.to[1];
                    var sizeZ = cube.from[2] - cube.to[2];

                    var newSizeX = Math.round(sizeX);
                    var newSizeY = Math.round(sizeY);
                    var newSizeZ = Math.round(sizeZ);
                    
                    var diffX = newSizeX - sizeX;
                    var diffY = newSizeY - sizeY;
                    var diffZ = newSizeZ - sizeZ;
                    cube.resize(diffX, 0, false, false);
                    cube.resize(diffY, 2, false, false);
                    cube.resize(diffZ, 3, false, false);

                });
                Undo.finishEdit("Make Decimal", {elements: Cube.all});
                }
            });
    
            genHex = new Action('control_gen_hex', {
                name: 'Generate Hex',
                icon: 'fa-vector-square',
                click: function(){
                    
                    new Dialog('control.generateHex', {
                        title:'Generate Shape',
                        form:{
                            sideLength: {
                                type: 'number',
                                label: 'Length of the shape\'s side',
                                value: 4
                            },
                            sides:{
                                label: 'How many sides?',
                                type: 'number',
                                value: 6
                            },
                            height: {
                                label: 'Height of the cubes',
                                type: 'number',
                                value: 1
                            },
                            x_rot:{
                                type: 'number',
                                value: 0,
                                label: 'X rotation'
                            }
                        },
                        onConfirm: function(data){
                            generateShape(data.sideLength, data.sides, data.height, data.x_rot);
                        }
                    }).show();

                }
            });
    
            makeControl = new Action('add_control', {
                name: 'Create TARDIS Control',
                icon: 'stadia_controller',
                click: function(){
    
                    new Dialog('control_set_control', {
                        title: 'TARDIS Control Plugin',
                        icon: 'fa.fa-police-box',
                        form: {
                            control: {
                                label: 'Control Type',
                                type: 'select',
                                options: ControlOptions
                            },
                            control_freetext:{
                                label: "Or Enter Control RL",
                                type: 'text'
                            }
                        },
                        onConfirm(data){

                            var name;
                            if(data.control_freetext && data.control_freetext !== ""){
                                name = data.control_freetext;
                            }
                            else{
                                name = data.control;
                            }
                            
                            var prettyName = name in ControlOptions ? ControlOptions[name] : name;

                            createTardisControlCube({
                                name: name,
                                prettyName: prettyName
                            });
                        }
                    }).show();
                }
            });

            MenuBar.addAction(tardisMenu = {
                id: 'tardis',
                name:"TARDIS Mod",
                icon: 'home_repair_service',
                children:[
                    makeIntAction,
                    genHex,
                    makeControl,
                    addSonicCPButton
                ]
            }, 'tools');

        }
    });
})();

function fixModelString(fullPath){
    var modid = /(?:assets)\\([a-z]*)\\(?:models)\\/.exec(fullPath)[1];
    var pathAfter = /(?:assets\\[a-z]*\\models\\)([^"|']*)/.exec(fullPath)[1];
    return (modid + ":" + pathAfter).replace(/[\\]/gm, "/").replace(/(?:\.json)$/gm, "");
}

function exportSonicCP(model, slot){
    var string = "{\n\t\"model\": \"" + fixModelString(model) + "\",\n\t"
    string += "\"part_slot\" : \"" + slot + "\",\n\t"
    string += "\"connection_points\": {\n"

        var connectionPoints = Cube.all.filter(c => c.sonicPartCubeProperty && c.sonicPartCubeProperty != "");
        var index = 0;
        connectionPoints.forEach(c => {
            string += "\t\t\"" + c.sonicPartCubeProperty + "\" : [ "
            
            string += (c.from[0] / 16.0) + ", "
            string += (c.from[1] / 16.0) + ", "
            string += (c.from[2] / 16.0) + " ]"

            if(index < connectionPoints.length - 1){
                string += ", \n"
            }

            ++index;
        });

    string += "\n\t}\n}"
    return string;
}

function exportControls(name){
    var data = "{\n\t\"console\":\"" + name + "\",\n\t\"controls\": {\n"
    var index = 0;

    var allCubes = Cube.all.filter(c => c.tardis_control && c.tardis_control !== "");
    allCubes.forEach(function(cube){

        var sizeX = (cube.to[0] - cube.from[0]) * 0.0625;
        var sizeY = (cube.to[1] - cube.from[1]) * 0.0625;
        var sizeZ = (cube.to[2] - cube.from[2]) * 0.0625;

        var posX = (cube.from[0] * 0.0625);// + (sizeX / 2.0);
        var posY = (cube.from[1] * 0.0625);// + (sizeY / 2.0);
        var posZ = (cube.from[2] * 0.0625);// + (sizeZ / 2.0);

        data += "\t\t\"" + cube.tardis_control + "\":{\n\t\t\t\"size\": [" + sizeX + ", " + sizeY + ", " + sizeZ +"],\n \t\t\t\"offset\":[" + posX + ", " + posY + ", " + posZ + "]"
        
        data += "\n\t\t}";
        
        if(index < allCubes.length - 1){
            data += ",";
        }
        data += "\n";

        ++index;

    });
    data = data + "\t}\n}";
    return data;
}

function generateShape(sideLength, sides, height, x_rot){
    const mainGroup = new Group({
        name: "shape"
    }).init()

    const halfLength = sideLength / 2.0;
    const deltaAngle = 360.0 / sides;

    var angle = 0.0;
    for(var i = 0; i < sides; ++i){

        const folder = new Group({
            name: 'panel_' + i,
            origin: [0, 0, 0],
            rotation: [x_rot, angle, 0],
        }).addTo(mainGroup).init();

        const cube = new Cube({
            name:'hex',
            from: [-halfLength, 0, sideLength - 2.0],
            to: [halfLength, height, sideLength - 1.0],
        }).addTo(folder).init();

        angle += deltaAngle;
    }
}

function fixControl(cube){
    cube.extend({
        rotation: [0, 0, 0],
        inflate: 0,
        autoUV: false,
        color: 0xFF0000FF
    });
    cube.applyTexture(undefined, true);

    //Move to root
    if(cube.parent !== 'root'){
        cube.addTo(undefined);
    }
    cube.export = false;

    Canvas.updateAll();
    
}

function createTardisControlCube(data){

    const cube  = new Cube({
        name: data.prettyName,
        from: [0, 20, 0],
        to: [1, 21, 1]
    });
    cube.tardis_control = data.name;
    cube.init();
}